<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'API\LoginController@login');


Route::post('register', 'API\RegisterController@register');

Route::middleware('auth:api')->group( function () {
	Route::post('logout','API\LoginController@logout');
	Route::group(['prefix' => 'v1'], function(){
		Route::get('posts', [
					'uses'	=>	'API\PostController@index',
					'as'	=>	'get-posts'
				]);

		Route::post('post/save', [
					'uses'	=>	'API\PostController@store',
					'as'	=>	'save-post'
				]);

		Route::post('post/update', [
					'uses'	=>	'API\PostController@update',
					'as'	=>	'update-posts'
				]);

		Route::post('post/delete', [
					'uses'	=>	'API\PostController@destroy',
					'as'	=>	'delete-post'
				]);

	});
	
});
