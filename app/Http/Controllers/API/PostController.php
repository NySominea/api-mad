<?php

namespace App\Http\Controllers\API;

use DB;
use Validator;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\API\BaseController as BaseController;

class PostController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts  =   Auth::user()->posts;

        return $this->sendResponse($posts, 'Posts retrieved successfully.');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $validator = Validator::make(request()->all(), [
            'title' => 'required',
        ]);
        
        if($validator->fails()){
            foreach(json_decode($validator->errors(),true) as $key => $error){
                $errors[$key] = reset($error); 
            }
            return $this->sendError('Validation Error.', $errors, 403);      
        }

        DB::beginTransaction();
        try{
            $input = request()->all();
            $input['user_id'] = Auth::user()->id;
            $post = Post::create($input);
            DB::commit();
            return $this->sendResponse($post, 'Post added successfully.');
        }
        catch(Exception $ex){
            DB::rollback();
            return $this->sendError('Server Error',['The post cannot be stored.'], 500);
        }
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update()
    {
        $post   =   Post::find(request()->get('postId'));
        
        if(!$post){
            return $this->sendError('Not Found',['This post cannot be found.']);
        }

        if($post->user_id != Auth::user()->id){
            return $this->sendError('No Permission',['You have no permission to update this post.'], 403);
        }

        DB::beginTransaction();
        try{
            $post->update(request()->all());
            DB::commit();
            return $this->sendResponse($post, 'This post updated successfully.');
        }
        catch(Exception $ex)
        {
            DB::rollback();
            return $this->sendError('Server Error',['This post cannot be updated.'], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        $post   =   Post::find(request()->get('postId'));

        if(!$post){
            return $this->sendError('Not Found',['This post cannot be found.']);
        }

        if($post->user_id != Auth::user()->id){
            return $this->sendError('No Permission',['You have no permission to delte this post.'], 403);
        }

        DB::beginTransaction();
        try{
            $post->delete();
            DB::commit();
            return $this->sendResponse($post, 'This post deleted successfully.');
        }
        catch(Exception $ex){
            DB::rollback();
            return $this->sendError('Server Error',['This post cannot be deleted.'], 500);
        }
    }
}
