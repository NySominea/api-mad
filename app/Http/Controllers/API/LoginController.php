<?php

namespace App\Http\Controllers\API;

use App\User;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\BaseController as BaseController;

class LoginController extends BaseController
{
    /**
     * Login api.
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
    	$validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        if($validator->fails()){
            foreach(json_decode($validator->errors(),true) as $key => $error){
                $errors[$key] = reset($error); 
            }
            return $this->sendError('Validation Error.', $errors, 403);      
        }
        
        $credentials = request(['email', 'password']);
        
        if(!Auth::attempt($credentials ,$request->filled('remember'))){
            return  $this->sendError('Unauthorized', ['You have entered an invalid username or password'], 401);
        }

        $user = $request->user();
        $accessToken = $user->createToken('MyApp')->accessToken;

        return $this->sendResponse(['user' => $user, 'accessToken' => $accessToken], 'User Login successfully.');
    }

    public function logout(){
        Auth::user()->token()->revoke();
        
        return $this->sendResponse('','User Logout successfully.');
    }
}
